all: repo init-repo public build-doc doc-index

repo:
	git clone https://gitlab.com/mimir-project/mimir.git repo

init-repo:
	cd repo && ./autogen.sh --enable-doc

public:
	mkdir -p public/doc/latest
	echo "<html><head><meta http-equiv='refresh' content='0; url=doc/latest'></head></html>" > public/index.html

build-doc: latest tagged

latest:
	./generate-latest
tagged:
	./generate-docs

doc-index:
	cd public/doc && tree -H '.' -L 1 -d --noreport --charset utf-8 -T doc/ > index.html
