mimir-project.gitlab.io
=======================
This repository is used to generate the GitLab Page:
https://mimir-project.gitlab.io

The site hosts the documentation of the mimir-project. The documentation
for releases and the latest master branch are provided.

https://mimir-project.gitlab.io/ will redirect to the latest documentation.
For a specific version visit: https://mimir-project.gitlab.io/doc

Rebuilding
----------
Push and tag events in https://gitlab.com/mimir-project/mimir trigger
rebuilding of the site using CI triggers.
